# Regolamento F1TM

Aspettare l'ok degli admin 
prima di confermare per l'inizio della gara (questo per evitare che la sessione parti quando ancora non è pronta) pena stella comportamentale

A fine gara dovrete fare eventuali segnalazioni entro Mercoledi alle 16.00 nella apposita sezione Discord - Report Incidenti - per questo è importantissimo registrare la propria gara.
Tutto ciò che non verrà postato nella apposita sezione entro la scadenza non verrà valutato.

Se i file sono troppo grandi per i report le soluzioni sono due:
- se è una live su Twitch mettete il minutaggio e il link oppure caricate la clip di quest'ultima
- oppure si può caricare il video su youtube e lo mettete in ''non in elenco'' cosi lo vedono solo quelli con il link



Incidente di gara = incidenti di gara minori non voluti con colpa 50 e 50

PENALITA' AGGIUNTIVE 
(nel caso in cui il gioco non le rilevasse)

Segnalini : 
Penalità lievissime / bandiera bianco e nera = fare 3 cambi di posizione in pista / incidenti di gara dubbi con votazione sul gruppo positiva / incidenti lievi / guida pericolosa
2 segnalini o ammonizioni : + 5 gara successiva
4 segnalini o ammonizioni : +10 gara successiva
6 segnalini o ammonizioni : partenza ultimo
8 segnalini o
ammonizioni : squalifica di 1 gara (ci sarà una SQ)
dopo l'ottavo segnalino si ricomincia dal primo

-segnalino per chi mette la presenza e poi sparisce senza dare spiegazioni
-segnalino quando per due volte di seguito non viene messa la presenza senza giustificazioni entro il giorno prima, se siete assenti mettete la X entro il giorno prima

I segnalini valgono anche da riserva e influiranno nella propria scheda/penalità pilota
-
+3s (taglio curva)
+5s (contatto lieve che non rovina totalmente la gara altrui con colpa al 100% - lieve danno)
+10s (contatto grave che rovina la gara altrui con colpa al 100 % - danno grave)
+15s (contatto grave che rovina la gara altrui con colpa e voluto al 100 % - danno grave)

Penalità corrispondente al tempo relativo alla pit lane della pista per incidente grave in safety car o partenza anticipata

È vietato il ''reimposta pista'' e l'uso dei bot in qualsiasi caso anche in Safety Car (salvo crash) : pena squalifica (SQ) prossima gara con partenza ultimi.
Usare i bot durante la qualifica comporta il segnalino.
Concludere la gara con il bot comporta l'annullamento della gara
Vietato ritirarsi in mezzo la pista pena la SQ ,
 il ritiro va fatto in sicurezza ai Box

Il volante nell’onboard dovrà essere sbloccato per analizzare meglio 
e capire cosi quanto e quando si curva

Usare tag di altri campionati o 
non rispettare le penalità comporta 
l'ANNULLAMENTO della gara
Estremamente consigliato usare il nostro tag F1TM

*Squalifica = Due incidenti gravi (da 15s o 10s) 
Tre incidenti lievi = partire ultimi nella prossima gara

Squalifiche (SQ) :
(verranno segnati nell'excel con le lettere SQ)

1 volta : ultimi prossima gara
2 volta : ultimi prossima gara
3 volta : squalifica prossima gara
4 volta : eliminazione dal torneo

Bisogna ritirarsi in qualifica nel caso si avesse la penalità di partire ultimi

Nel caso in cui ci sono più macchine che partono ultime l'ordine di partenza delle ultime caselle sarà stabilito in ordine di gravità. Bisognerà ritirarsi in qualifica in ordine di chiamata. 

In caso di bug di gioco sarà necessario mostrare il video, nel caso in cui ci assicuriamo che sia stata colpa del gioco non sarà inflitta nessuna penalità
In caso di perdita di dati con tanto di connessione scarsa che provoca incidenti, dopo il primo incidente sarà obbligatorio il ritiro altrimenti le penalità verranno applicate normalmente a proprio rischio e pericolo. Se si ha ping sopra 150 NON bisogna partecipare, pena stella comportamentale

---------
Warning Comportamentali
(verranno segnati nell'excel con la lettera W e sono considerabili delle SQ)

1 Partenza Ultimo
2 SQ di una Gara
3 SQ dal Campionato

Motivi del Warning 

1 stella : non ammettere i propri errori / lamentarsi / essere impazienti / insulti lievi / eccessivi ritardi e assenze / non avvisare della propria assenza (il Warning scatta a seguito della decisione degli admin dopo comportamenti da 1 stella ripetuti)

2 stelle : insulti gravi / barare / non rispettare le proprie penalità / trolling grave (Warning Diretto con i comportamenti da 2 stelle)

il comportamento vale sia in chat/vocale/pista
